//ТЕОРЕТИЧНІ ПИТАННЯ
// 1. Метод forEach - приймає всередину колбек функцію, яку виконує для кожного елемента масиву. Він потрібен для перебору значень у масиві.
// 2. Можна задати нульову довжину масиву (length). Або використати метод splise (одна з його властивостей -  видаляти з поточного масиву елементи).
// 3. Array.isArray(змінна). Має повернути true або false.

//ПРАКТИЧНЕ ЗАВДАННЯ

let array = ["hello", "world", 23, "23", null];
function filterBy(arr, arrType) {
  return arr.filter((n) => typeof n !== arrType);
}
let newArr = filterBy(array, "string");
console.log(newArr);
